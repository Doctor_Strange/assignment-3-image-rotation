#ifndef BMP_SERIALIZER_H
#define BMP_SERIALIZER_H

#include "../include/image.h"

#include <stdint.h>
#include <stdio.h>

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* img );
#endif//BMP_SERIALIZER_H
