#ifndef BMP_DESERIALIZER_H
#define BMP_DESERIALIZER_H

#include "../include/image.h"

#include <stdint.h>
#include <stdio.h>

enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER
  };

enum read_status from_bmp( FILE* in, struct image* img );

#endif//BMP_DESERIALIZER_H
