#ifndef UTIL_H
#define UTIL_H

#include "../include/bmp_deserializer.h"
#include "../include/bmp_serializer.h"
#include "../include/image.h"
#include "../include/pixel.h"
#include "../include/rotator.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int rotate_image(char* image_path, char* rot_image_path, int angle);
int parse_arguments(int argc, char *argv[], char **image_path, char **rot_image_path, int *angle);
int angle_validator(const char *arg);
#endif//UTIL_H
