#ifndef IMAGE_H
#define IMAGE_H


#include "../include/rotator.h"

#include <stdint.h>

struct image {
  uint64_t width, height;
  struct pixel* data;
};

#endif//IMAGE_H
