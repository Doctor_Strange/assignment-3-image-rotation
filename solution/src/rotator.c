#include <stdlib.h>

#include "../include/image.h"
#include "../include/pixel.h"

struct image rotate(struct image const source) {
    struct image image_rot;
    image_rot.width = source.height;
    image_rot.height = source.width;
    image_rot.data = (struct pixel*)malloc(sizeof(struct pixel) * image_rot.width * image_rot.height);

    if (image_rot.data == NULL)
    {
        struct image empty_image = {0, 0, NULL};
        return empty_image;
    }

    for (uint64_t i = 0; i < source.height; i++) {
        for (uint64_t j = 0; j < source.width; j++)
            image_rot.data[(image_rot.height - j - 1) * image_rot.width + i] = source.data[i * source.width + j];
    }

    free(source.data);
    return image_rot;
}
