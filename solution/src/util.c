#include <stdio.h>
#include <stdlib.h>

#include "../include/bmp_serializer.h"
#include "../include/bmp_deserializer.h"
#include "../include/util.h"

const size_t EXPECTED_ARGS = 4;
const int ANGLES[] = {0, 90, -90, 180, -180, 270, -270};
const int LEN_ANGLES = 7;
const int INVALID_ANGLE = -1;

int rotate_image(char* image_path, char* rot_image_path,int angle)
{
  FILE* source_file = fopen(image_path, "rb");
  if (!source_file) {
    fprintf(stderr, "Error: Could not open source image: %s\n", image_path);
    return 1;
  }

  struct image source_image;
  enum read_status read_result = from_bmp(source_file, &source_image);
  fclose(source_file);

  if (read_result != READ_OK) {
    free(source_image.data);
    fprintf(stderr, "Error: Failed to read BMP file.\n");
    return 1;
  }

  int rotations = (abs(angle) % 360) / 90;

  if (angle < 0)
    rotations = 4 - rotations;

  for (int i = 0; i < rotations; i++)
  {
      struct image rotated_image = rotate(source_image);
      if (rotated_image.data == NULL) {
          free(source_image.data);
          fprintf(stderr, "Error: Failed to rotate the image.\n");
          return 1;
      }

      source_image = rotated_image;
  }


  FILE* dest_file = fopen(rot_image_path, "wb");
  if (!dest_file) {
    fprintf(stderr, "Error: Could not open destination image: %s\n", rot_image_path);
    free(source_image.data);
    return 1;
  }

  enum write_status write_result = to_bmp(dest_file, &source_image);
  fclose(dest_file);

  if (write_result != WRITE_OK) {
    fprintf(stderr, "Error: Failed to write BMP file.\n");
    free(source_image.data);
    return 1;
  }

  free(source_image.data);
  return 0;
}

int angle_validator(const char *arg)
{
    int angle = atoi(arg);

    for (int i = 0; i < LEN_ANGLES; i++)
    {
        if (angle == ANGLES[i])
        {
            return angle;
        }
    }

    fprintf(stderr, "Error: Invalid angle provided. Use only: 0, 90, -90, 180, -180, 270, -270\n");
    return INVALID_ANGLE;
}

int parse_arguments(int argc, char *argv[], char **image_path, char **rot_image_path, int *angle)
{
    if (argc != EXPECTED_ARGS)
    {
        fprintf(stderr, "Usage: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        return 1;
    }

    *image_path = argv[1];
    *rot_image_path = argv[2];
    *angle = angle_validator(argv[3]);
    if (*angle == INVALID_ANGLE)
        return 1;
    return 0;
}
