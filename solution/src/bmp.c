#include <stdio.h>
#include <stdlib.h>

#include "../include/bmp_serializer.h"
#include "../include/bmp_deserializer.h"
#include "../include/bmp_header.h"
#include "../include/pixel.h"


#define BMP_TYPE 0x4D42
#define BMP_BIT 24
#define BMP_RESERVED 0
#define BMP_PLANES 1
#define BMP_SIZE 40
#define BMP_COMPRESSION 0
#define BMP_X_PELS_PER_METER 0
#define BMP_Y_PELS_PER_METER 0
#define BMP_CLR_USED 0
#define BMP_CLR_IMPORTANT 0
#define PADDING 4


static uint8_t calculate_padding(uint64_t length) {
    return (PADDING - (length % PADDING) % PADDING);
}

enum read_status from_bmp(FILE *in, struct image *img)
{
    struct bmp_header header;

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
        return READ_INVALID_HEADER;

    if (header.bfType != BMP_TYPE)
        return READ_INVALID_SIGNATURE;

    if (header.biBitCount != BMP_BIT)
        return READ_INVALID_BITS;

    img->width = header.biWidth;
    img->height = header.biHeight;
    size_t pixel_data_size = img->width * img->height * sizeof(struct pixel);
    img->data = malloc(pixel_data_size);
    if (!img->data)
        return READ_INVALID_HEADER;

    const uint8_t padding = calculate_padding(img->width * sizeof(struct pixel));

    for (uint64_t i = 0; i < img->height; i++) {
        if (fread(&img->data[i * img->width], sizeof(struct pixel), img->width, in) != img->width) {
            free(img->data);
            return READ_INVALID_HEADER;
        }
        fseek(in, padding, SEEK_CUR);
    }


    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img)
{
    const uint8_t BYTES_PER_PIXEL = sizeof(struct pixel);
    const uint8_t padding = calculate_padding(img->width * sizeof(struct pixel));
    const uint32_t pixel_data_size = img->width * img->height * BYTES_PER_PIXEL;
    const uint32_t file_size = sizeof(struct bmp_header) + pixel_data_size + img->height * padding;

    struct bmp_header header =
            {
            .bfType = BMP_TYPE,
            .bfileSize = file_size,
            .bfReserved = BMP_RESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMP_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BMP_PLANES,
            .biBitCount = BMP_BIT,
            .biCompression = BMP_COMPRESSION,
            .biSizeImage = pixel_data_size,
            .biXPelsPerMeter = BMP_X_PELS_PER_METER,
            .biYPelsPerMeter = BMP_Y_PELS_PER_METER,
            .biClrUsed = BMP_CLR_USED,
            .biClrImportant = BMP_CLR_IMPORTANT };

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
        return WRITE_ERROR;

    for (uint64_t i = 0; i < img->height; i++) {
        if (fwrite(&img->data[i * img->width], BYTES_PER_PIXEL, img->width, out) != img->width)
            return WRITE_ERROR;
        for (uint8_t j = 0; j < padding; j++) {
            if (fputc(0, out) == EOF) {
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}
