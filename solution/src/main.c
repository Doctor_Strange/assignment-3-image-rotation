#include <stdio.h>

#include "../include/util.h"

int main(int argc, char **argv)
{
    char *image_path, *rot_image_path;
    int angle;

    if (parse_arguments(argc, argv, &image_path, &rot_image_path, &angle) != 0)
        exit(1);

    if (rotate_image(image_path, rot_image_path, angle) != 0)
        exit(1);
}
